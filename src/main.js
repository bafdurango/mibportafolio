import Vue from 'vue'
import Antd from 'ant-design-vue'
import App from './App.vue'
import 'ant-design-vue/dist/antd.css'
import Router from 'vue-router'

import axios from 'axios'

//axios.defaults.baseURL = 'http://127.0.0.1:8000';
//axios.defaults.baseURL = 'http://localhost/mibportafoliodrupal';

Vue.config.productionTip = false

import Proyectos from './components/Proyectos.vue'
import Signin from './components/Signin.vue'
import Post from './components/Post.vue'

Vue.use(Antd)
Vue.use(Router)
//Vue.use(axios)

const routes = [
  { path: '/home', component: Proyectos },
  { path: '/signin', component: Signin },
  { path: '/post/:id', component: Post },
]

const router = new Router({
  routes // short for `routes: routes`
})

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
